import OpenAI from 'openai';

const openai = new OpenAI({
  apiKey: process.env.OPENAI_API_KEY,
});

export class PromptDemo {
  constructor() {
    this.process();
  }

  process() {
    const openai = this.initOpenAi();
    const response = openai.chat.completions.create({
      model: 'gpt-3.5-turbo',
      messages: [
        {
          role: 'system',
          content:
            'You will be provided with Python code, and your task is to calculate its time complexity.',
        },
        {
          role: 'user',
          content:
            'def foo(n, k):\n        accum = 0\n        for i in range(n):\n            for l in range(k):\n                accum += i\n        return accum',
        },
      ],
      temperature: 0.7,
      max_tokens: 64,
      top_p: 1,
    });

    response.then((data) => {
      console.log('response=', data);
    });
  }

  initOpenAi(): OpenAI {
    return new OpenAI({
      apiKey: process.env.OPENAI_API_KEY,
    });
  }
}

new PromptDemo();
